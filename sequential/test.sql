

/*
setup audit tables
*/

drop table if exists "TEST_DB"."PUBLIC"."DQ_AUDIT_LOG";

create table "TEST_DB"."PUBLIC"."DQ_AUDIT_LOG"
(
  RUN_ID int,
  TEST_CASE VARCHAR(255),
  
  notes varchar(255),
  pass_fail varchar(20),
  METRIC_1 double,
  metric_2 double
);

-- test case 1

insert into "TEST_DB"."PUBLIC"."DQ_AUDIT_LOG"
select run_id, test_case, case when metric1 = metric2 then 'row counts matched' when metric1 != metric2 then 'bad row counts' end as notes, case when metric1=metric2 then 'PASS' else 'FAIL' end as pass_fail, metric1, metric2 
from
(
select 1 as run_id, 'Row count of DATA_SRC table' as test_case, sum(metric1) as metric1, sum(metric2) as metric2
from (
select 1 as run_id, 'Row Count of DATA_SRC table' as test_case, count(*) as metric1, 0 as metric2 from DATA_SRC
union all
select 1, 'Row Count of DATA_SRC table' as test_case, 0 as metric1, count(*) as metric2 from DATA_SRC
  )
);
